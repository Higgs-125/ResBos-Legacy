#!/bin/bash
WORKDIR=$PWD
multicore=0
localrun=0
PDFNumber=2
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=15
InitialProcess=1
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

WHICHPDF=${MYPDF}_pds
NBosons=2

for((Boson=1;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $Boson -eq 1 ]; then
myBOSON=zu
elif [ $Boson -eq 2 ]; then
myBOSON=zd
elif [ $Boson -eq 3 ]; then
myBOSON=wp
elif [ $Boson -eq 4 ]; then
myBOSON=wm
fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFDIR=${PDFFile}0${k}_${myBOSON}
else
PDFDIR=${PDFFile}${k}_${myBOSON}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFDIR=${PDFFile}00_${myBOSON}
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleDIR=Scale908
fi

YGRID=$WORKDIR/../y_piece/${WHICHPDF}/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
PERT=$WORKDIR/../w_pert/${WHICHPDF}/${PDFDIR}/$ScaleDIR/JOB1/w_pert.out
ASYM=$WORKDIR/../w_asym/${WHICHPDF}/${PDFDIR}/$ScaleDIR/JOB1/w_asym.out
YKGRID=${PDFDIR}_${ScaleDIR}_yk.out
ln -s $YGRID legacy_y.out
ln -s $PERT w_pert.out
ln -s $ASYM w_asym.out
./get_yk w_pert.out w_asym.out legacy_y.out $YKGRID
rm legacy_y.out w_pert.out w_asym.out
echo ${PDFDIR} ${ScaleDIR} has been generated.

done

done
