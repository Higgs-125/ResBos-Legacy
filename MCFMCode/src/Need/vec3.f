      subroutine unitvec(p_in, p_out)
      implicit none
      include 'types.f'
      real(dp):: p_in(3), p_out(3), total
      total = sqrt(p_in(1)**2 + p_in(2)**2 + p_in(3)**2)
      p_out(1) = p_in(1)/total
      p_out(2) = p_in(2)/total
      p_out(3) = p_in(3)/total
      end

      subroutine cross3(p1, p2, p_out)
      implicit none
      include 'types.f'
      real(dp):: p1(3), p2(3), p_out(3)

      p_out(1) = p1(2)*p2(3) - p1(3)*p2(2)
      p_out(2) = p1(3)*p2(1) - p1(1)*p2(3)
      p_out(3) = p1(1)*p2(2) - p1(2)*p2(1)
      end

      function angle(p1,p2)
      implicit none
      include 'types.f'
      real(dp):: p1(3), p2(3), angle, dot, l1, l2, dot3
      external dot3

      dot = dot3(p1,p2)
      l1 = dot3(p1,p1)
      l2 = dot3(p2,p2)

      angle = acos(dot/sqrt(l1*l2))

      return
      end

      function dot3(p1,p2)
      implicit none
      include 'types.f'
      real(dp):: p1(3), p2(3), dot3

      dot3 = p1(1)*p2(1)+p1(2)*p2(2)+p1(3)*p2(3)

      return
      end

