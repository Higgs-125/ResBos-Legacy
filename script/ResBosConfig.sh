#!/bin/bash
WConfig=$1
YConfig=$2
isOutput=$3

if [ $WConfig -eq 0 ]; then
    YConfig=0
fi
if [ $WConfig -eq -1 ]; then
    YConfig=-1
fi

if [ $isKinCorr -eq 1 ]; then
    if [ $KinCorr -eq 1 ]; then
        KinCorrFlag=KinCorrOn
    elif [ $KinCorr -eq 0 ]; then
        KinCorrFlag=KinCorrOff
    fi
else
    KinCorrFlag=
fi

###Set W-piece gridfile path
if [ $WConfig -eq 1 ]; then
    if [ $isRotation -ne 1 ]; then
        if [ $isLegacy -eq 1 ]; then
            WGridPath=$WORKDIR/../w_piece/${KinCorrFlag}/${MYPDF}_MT_pds/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
            WName=Legacy_w321
        elif [ $isResBos2 -eq 1 ]; then
            #WGridPath=$WORKDIR/../ResBos2/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_w321_pds/${PDFDIR}/$ScaleDIR/JOB1/Grids/ResBos_GridFile_0.out
            WGridPath=$WORKDIR/../ResBos2/${KinCorrFlag}/${MYPDF}_Q_w321_pds/${PDFDIR}/$ScaleDIR/JOB1/Grids/ResBos_GridFile_0.out
            WName=ResBos2_w321
        fi
    elif [ $isRotation -eq 1 ]; then
        WGridPath=$WORKDIR/../w_piece/${MYPDF}_rotated/$ObservableName/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
        WName=Legacy_w321
    fi
elif [ $WConfig -eq 2 ]; then
    #WGridPath=$WORKDIR/../ResBos2/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_w432_pds/${PDFDIR}/$ScaleDIR/JOB1/Grids/ResBos_GridFile_0.out
    WGridPath=$WORKDIR/../ResBos2/${KinCorrFlag}/${MYPDF}_Q_w432_pds/${PDFDIR}/$ScaleDIR/JOB1/Grids/ResBos_GridFile_0.out
    WName=ResBos2_w432
elif [ $WConfig -eq 0 ]; then
    WGridPath=$WORKDIR/../delsig_piece/${MYPDF}_pds/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
    WName=delsig
elif [ $WConfig -eq -1 ]; then
    WGridPath=$WORKDIR/../asy_piece/${MYPDF}_pds/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
    WName=asy
fi

###Set Y-piece gridfile path
if [ $YConfig -eq 1 ]; then
    ISYGRID=legacy_yk.out
    if [ $isRotation -ne 1 ]; then
        YGridPath=$WORKDIR/../y_piece/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_pds/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
#        YGridPath=$WORKDIR/../get_yk/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_pds_NLO/${PDFDIR}_${ScaleDIR}_yk.out
#        YGridPath=$WORKDIR/../get_yk/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_pds_YPiece/${PDFDIR}_${ScaleDIR}_y.out
    elif [ $isRotation -eq 1 ]; then
        YGridPath=$WORKDIR/../old_y_piece/${MYPDF}_rotated/$ObservableName/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
    fi
    YName=y1
elif [ $YConfig -eq 2 ]; then
    ISYGRID=legacy_yk.out
#    YGridPath=$WORKDIR/../get_yk/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_pds/${PDFDIR}_${ScaleDIR}_yk.out
#    YGridPath=$WORKDIR/../get_yk/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_pds_NNLO/${PDFDIR}_${ScaleDIR}_yk.out
    YGridPath=$WORKDIR/../get_yk/${KinCorrFlag}/${MYPDF}${SCALECHOICE}_pds_YPiece/${PDFDIR}_${ScaleDIR}_yk.out
    YName=y2
elif [ $YConfig -eq 0 ]; then
    ISYGRID="-"
    YGridPath=$WORKDIR/../y_piece/${MYPDF}${SCALECHOICE}_pds/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
    YName=y0
elif [ $YConfig -eq -1 ]; then
    ISYGRID=legacy_yk.out
    YGridPath=$WORKDIR/../y_piece/${MYPDF}_pds/${PDFDIR}/$ScaleDIR/JOB1/legacy.out
    YName=y0
fi

if [ $isOutput -eq 1 ]; then

    WHICHPDF=${MYPDF}_${ECM}_${WName}_${YName}

    if [ $WConfig -eq 0 -o $WConfig -eq -1 ]; then
        WHICHPDF=${MYPDF}_${ECM}_${WName}
    fi

    if [ $PseudoData -eq 1 ]; then
        WHICHPDF=PseudoData_${STWINPUT}_${MYPDF}
    fi

    if [ $TryOld -eq 1 ]; then
        WHICHPDF=${WHICHPDF}_TryOld
    fi

    if [ $Process -eq 3 ]; then
        WHICHPDF=Template_${WHICHPDF}
    fi

    if [ $isRotation -eq 1 ]; then
        WHICHPDF=${WHICHPDF}_rotated
    fi

    if [ $isKinCorr -eq 1 ]; then
        WHICHPDF=${WHICHPDF}_${KinCorrFlag}
    fi
######output######

    if [ $WConfig -eq 1 -a $YConfig -eq 1 ]; then
        echo w321 + y calculation
        echo " "
    elif [ $WConfig -eq 1 -a $YConfig -eq 2 ]; then
        echo w321 + yk calculation
        echo " "
    elif [ $WConfig -eq 1 -a $YConfig -eq 0 ]; then
        echo only w321 calculation
        echo " "
    elif [ $WConfig -eq 2 -a $YConfig -eq 1 ]; then
        echo w432 + y calculation
        echo " "
    elif [ $WConfig -eq 2 -a $YConfig -eq 2 ]; then
        echo w432 + yk calculation
        echo " "
    elif [ $WConfig -eq 2 -a $YConfig -eq 0 ]; then
        echo only w432 calculation
        echo " "
    elif [ $WConfig -eq 0 -a $YConfig -eq 0 ]; then
        echo delsig calculation
        echo " "
    elif [ $WConfig -eq -1 -a $YConfig -eq -1 ]; then
        echo asy + y calculation
        echo " "
    fi
fi
