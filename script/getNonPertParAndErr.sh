#!/bin/bash
NonPertMode=$1

if [ $NonPertMode -eq 1 ]; then  #before marginalizing

echo "The NonPertFit result is before marginalizing."

G1NUMBER=`grep 'Summary :   0)  Parameter' log.txt |awk '{print $7}'`
G2NUMBER=`grep 'Summary :   1)  Parameter' log.txt |awk '{print $7}'`
G3NUMBER=`grep 'Summary :   2)  Parameter' log.txt |awk '{print $7}'`
G4NUMBER=`grep 'Summary :   3)  Parameter' log.txt |awk '{print $7}'`
G5NUMBER=`grep 'Summary :   4)  Parameter' log.txt |awk '{print $7}'`
G6NUMBER=`grep 'Summary :   5)  Parameter' log.txt |awk '{print $7}'`
G1ERR=`grep 'Summary :   0)  Parameter' log.txt |awk '{print $9}'`
G2ERR=`grep 'Summary :   1)  Parameter' log.txt |awk '{print $9}'`
G3ERR=`grep 'Summary :   2)  Parameter' log.txt |awk '{print $9}'`
G4ERR=`grep 'Summary :   3)  Parameter' log.txt |awk '{print $9}'`
G5ERR=`grep 'Summary :   4)  Parameter' log.txt |awk '{print $9}'`
G6ERR=`grep 'Summary :   5)  Parameter' log.txt |awk '{print $9}'`
G1ERR=${G1ERR:0:6}
G2ERR=${G2ERR:0:6}
G3ERR=${G3ERR:0:6}
G4ERR=${G4ERR:0:6}
G5ERR=${G5ERR:0:6}
G6ERR=${G6ERR:0:6}
#CHI2=`grep 'Summary :  Log of the maximum posterior:' log.txt |awk '{print $8}'`
CHI2=`grep 'Summary :      Total Chi2:' log.txt |awk '{print $5}'`
CHI2_NPT=`grep 'Summary :      Chi2/N_dof:' log.txt |awk '{print $4}'`
Corr12=`grep 'Summary :   Corr(g_1, g_2) =' log.txt |awk '{print $6}'`
Corr13=`grep 'Summary :   Corr(g_1, g_3) =' log.txt |awk '{print $6}'`
Corr14=`grep 'Summary :   Corr(g_1, g_4) =' log.txt |awk '{print $6}'`
Corr15=`grep 'Summary :   Corr(g_1, g_5) =' log.txt |awk '{print $6}'`
Corr16=`grep 'Summary :   Corr(g_1, g_6) =' log.txt |awk '{print $6}'`
Corr23=`grep 'Summary :   Corr(g_2, g_3) =' log.txt |awk '{print $6}'`
Corr24=`grep 'Summary :   Corr(g_2, g_4) =' log.txt |awk '{print $6}'`
Corr25=`grep 'Summary :   Corr(g_2, g_5) =' log.txt |awk '{print $6}'`
Corr26=`grep 'Summary :   Corr(g_2, g_6) =' log.txt |awk '{print $6}'`
Corr34=`grep 'Summary :   Corr(g_3, g_4) =' log.txt |awk '{print $6}'`
Corr35=`grep 'Summary :   Corr(g_3, g_5) =' log.txt |awk '{print $6}'`
Corr36=`grep 'Summary :   Corr(g_3, g_6) =' log.txt |awk '{print $6}'`
Corr45=`grep 'Summary :   Corr(g_4, g_5) =' log.txt |awk '{print $6}'`
Corr46=`grep 'Summary :   Corr(g_4, g_6) =' log.txt |awk '{print $6}'`
Corr56=`grep 'Summary :   Corr(g_5, g_6) =' log.txt |awk '{print $6}'`
Corr12=${Corr12:0:5}
Corr13=${Corr13:0:5}
Corr14=${Corr14:0:5}
Corr15=${Corr15:0:5}
Corr16=${Corr16:0:5}
Corr23=${Corr23:0:5}
Corr24=${Corr24:0:5}
Corr25=${Corr25:0:5}
Corr26=${Corr26:0:5}
Corr34=${Corr34:0:5}
Corr35=${Corr35:0:5}
Corr36=${Corr36:0:5}
Corr45=${Corr45:0:5}
Corr46=${Corr46:0:5}
Corr56=${Corr56:0:5}

G1NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G1NUMBER`
G2NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G2NUMBER`
G3NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G3NUMBER`
G4NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G4NUMBER`
#CHI2=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $CHI2`
#CHI2_NPT=`echo "scale=2; $CHI2*(-2)/$NDATA" | bc`

elif [ $NonPertMode -eq 2 ]; then #after marginalizing

echo "The NonPertFit result is after marginalizing."

G1NUMBER=`grep 'Summary :       Mean +- sqrt(Variance):' log.txt |awk '{print $6}' |head -1 |tail -1`
G2NUMBER=`grep 'Summary :       Mean +- sqrt(Variance):' log.txt |awk '{print $6}' |head -2 |tail -1`
G3NUMBER=`grep 'Summary :       Mean +- sqrt(Variance):' log.txt |awk '{print $6}' |head -3 |tail -1`
G4NUMBER=`grep 'Summary :       Mean +- sqrt(Variance):' log.txt |awk '{print $6}' |head -4 |tail -1`

G1NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G1NUMBER`
G2NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G2NUMBER`
G3NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G3NUMBER`
G4NUMBER=`source ~/pku_resbos/FrameWork/script/SymbolExtract.sh $G4NUMBER`

fi

echo "NonPertFitting results:"
echo "g1 = $G1NUMBER +- $G1ERR"
echo "g2 = $G2NUMBER +- $G2ERR"
echo "g3 = $G3NUMBER +- $G3ERR"
echo "g4 = $G4NUMBER +- $G4ERR"
echo "g5 = $G5NUMBER +- $G5ERR"
echo "g6 = $G6NUMBER +- $G6ERR"
echo "Corr12 = $Corr12"
echo "Corr13 = $Corr13"
echo "Corr14 = $Corr14"
echo "Corr15 = $Corr15"
echo "Corr16 = $Corr16"
echo "Corr23 = $Corr23"
echo "Corr24 = $Corr24"
echo "Corr25 = $Corr25"
echo "Corr26 = $Corr26"
echo "Corr34 = $Corr34"
echo "Corr35 = $Corr35"
echo "Corr36 = $Corr36"
echo "Corr45 = $Corr45"
echo "Corr46 = $Corr46"
echo "Corr56 = $Corr56"
echo "Chi2 = $CHI2"
echo "Chi2/NpT = $CHI2_NPT"
