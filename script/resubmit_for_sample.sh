#!/bin/bash
WORKDIR=$PWD
GridDIR=/lustre/AtlUser/yfu/pku_resbos/gridfile
multicore=0
localrun=0
WConfig=0   ###1: w321  2: w432  0: delsig  -1: asy
YConfig=0   ###1: y1    2: y2    0: onlyW   -1: asy  ##For NLO calculation, please usage WConfig=0 YConfig=0, and then WConfig=-1 YConfig=-1, and hadd the result.
onlyCentral=0
doResubmit=$2
Count=0

PDFNumber=2
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 1
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi
############################################################################
NJobs=10
ECM=8TeV

Process=$1
if [ $Process -eq 1 ]; then
    echo Running scale variation.
    NProcess=15
    InitialProcess=1
elif [ $Process -eq 2 ]; then
    echo Running PDF uncertainty.
    NProcess=${NumberOfPDFSet}
    InitialProcess=0
fi

if [ $YConfig -eq 1 ]; then
    WHICHPDF=${MYPDF}_${ECM}_NLO
elif [ $YConfig -eq 2 ]; then
    WHICHPDF=${MYPDF}_${ECM}_NNLO
elif [ $YConfig -eq 0 ]; then
    WHICHPDF=${MYPDF}_${ECM}_onlyW
fi

if [ $WConfig -eq 0 ]; then
    WHICHPDF=${MYPDF}_${ECM}_delsig
elif [ $WConfig -eq -1 ]; then
    WHICHPDF=${MYPDF}_${ECM}_asy
fi

NBosons=2

for((Boson=1;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $Boson -eq 1 ]; then
    myBOSON=zu
elif [ $Boson -eq 2 ]; then
    myBOSON=zd
elif [ $Boson -eq 3 ]; then
    myBOSON=wp
elif [ $Boson -eq 4 ]; then
    myBOSON=wm
fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=${NProcess};k++)); do
if [ $k -lt 10 ]; then
    PDFDIR=${PDFFile}0${k}_${myBOSON}
else
    PDFDIR=${PDFFile}${k}_${myBOSON}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
    cd $WORKDIR
    PDFDIR=${PDFFile}00_${myBOSON}

elif [ $Process -eq 2 ]; then
    cd $WORKDIR
    ScaleDIR=Scale908
fi

echo " "
echo $PDFDIR $ScaleDIR
echo " "

for((n=1;n<=$NJobs;n++)); do

if [ `ll $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n/resbos.root |awk '{print $5}'` -lt 2180000000 ]; then
    echo $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
    Count=$[$Count+1]
    if [ $doResubmit -eq 1 ]; then
        cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
        condor_submit mySUB*
        cd $WORKDIR
    elif [ $doResubmit -eq 2 ]; then
        cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
        ./main
        wait
        cd $WORKDIR
    fi
fi

done

done ##done process

done ##done boson

echo " "
echo Number of Fail Jobs is $Count
echo " "

cd $WORKDIR/
