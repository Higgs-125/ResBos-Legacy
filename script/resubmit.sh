#!/bin/bash
WORKDIR=$PWD
GridDIR=/lustre/AtlUser/yfu/pku_resbos/gridfile
multicore=0
localrun=0
onlyCentral=0
doResubmit=$2

PDFNumber=2
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi

NJobs=`cat inp/q_grid_0.inp |wc -l`
Count=0

#NJobs=10
ECM=8TeV

Process=$1
if [ $Process -eq 1 ]; then
    echo Running scale variation.
    NProcess=15
    InitialProcess=1
elif [ $Process -eq 2 ]; then
    echo Running PDF uncertainty.
    NProcess=${NumberOfPDFSet}
    InitialProcess=0
fi

#WHICHPDF=${MYPDF}_${ECM}_NLO_FixedOrder
WHICHPDF=${MYPDF}_pds
NBosons=2

cd $WORKDIR

for((Boson=1;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $Boson -eq 1 ]; then
    myBOSON=zu
elif [ $Boson -eq 2 ]; then
    myBOSON=zd
elif [ $Boson -eq 3 ]; then
    myBOSON=wp
elif [ $Boson -eq 4 ]; then
    myBOSON=wm
fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=${NProcess};k++)); do
if [ $k -lt 10 ]; then
    PDFDIR=${PDFFile}0${k}_${myBOSON}
else
    PDFDIR=${PDFFile}${k}_${myBOSON}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
    cd $WORKDIR
    PDFDIR=${PDFFile}00_${myBOSON}

elif [ $Process -eq 2 ]; then
    cd $WORKDIR
    ScaleDIR=Scale908
fi

echo " "
echo $PDFDIR $ScaleDIR
echo " "

for((n=1;n<=$NJobs;n++)); do

if [ ! -f "$WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n/legacy.out" ]; then
    echo $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
    Count=$[$Count+1]
    if [ $doResubmit -eq 1 ]; then
        cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
        condor_submit mySUB*
        cd $WORKDIR
    elif [ $doResubmit -eq 2 ]; then
        cd $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/JOB$n
        ./main
        wait
        cd $WORKDIR
    fi
fi

done

done ##done process

done ##done boson

echo " "
echo Number of Fail Jobs is $Count
echo " "

cd $WORKDIR/
