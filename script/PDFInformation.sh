#!/bin/bash
PDFNumber=$1
if [ $PDFNumber -eq 1 ]; then
MYPDF=CT14HERA2NNLO
PDFFile=If1363.
NumberOfPDFSet=56
elif [ $PDFNumber -eq 2 ]; then
MYPDF=CT14nnlo
PDFFile=CT14nn.
NumberOfPDFSet=56
elif [ $PDFNumber -eq 3 ]; then
MYPDF=MMHT2014nnlo68cl
PDFFile=MMHT2_00
NumberOfPDFSet=50
elif [ $PDFNumber -eq 4 ]; then
MYPDF=CT18NNLO
PDFFile=i2Tn3.
NumberOfPDFSet=0
elif [ $PDFNumber -eq 5 ]; then
MYPDF=CT10
PDFFile=ct10.
NumberOfPDFSet=52
elif [ $PDFNumber -eq 6 ]; then
MYPDF=NNPDF30_nnlo_as_0118
PDFFile=NNPDF_00
NumberOfPDFSet=100 ##actually is 100, but when number of PDF set is larger than 99, all of the script need to be modified more
elif [ $PDFNumber -eq 7 ]; then
MYPDF=CT18NNLO
PDFFile=i2Tn3.
NumberOfPDFSet=58
elif [ $PDFNumber -eq 8 ]; then
MYPDF=NNPDF31_nnlo_as_0118_1000
PDFFile=NNPDF_00
NumberOfPDFSet=1000 ##actually is 100, but when number of PDF set is larger than 99, all of the script need to be modified more
elif [ $PDFNumber -eq 9 ]; then
MYPDF=cteq6
PDFFile=cteq6.
NumberOfPDFSet=40
elif [ $PDFNumber -eq 10 ]; then
MYPDF=cteq66
PDFFile=cteq66.
NumberOfPDFSet=44
elif [ $PDFNumber -eq 11 ]; then
MYPDF=NNPDF31_nnlo_as_0118
PDFFile=NNPDF31_nnlo_as_0118.
NumberOfPDFSet=100
elif [ $PDFNumber -eq 12 ]; then
MYPDF=FullAFB_RotatedCT18NNLO_LHA
PDFFile=FullAFB_RotatedCT18NNLO_LHA.
NumberOfPDFSet=2
fi
