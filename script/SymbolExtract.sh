#!/bin/bash
myFloat=$1

Symbol=`echo $myFloat |grep +`
if [[ "$Symbol" != "" ]]; then
myFloat=${myFloat:1:10}
fi

Symbol=`echo $myFloat |grep -`
if [[ "$Symbol" != "" ]]; then
myFloat=${myFloat:0:10}
fi

Symbol1=`echo $myFloat |grep -`
Symbol2=`echo $myFloat |grep +`
if [[ "$Symbol1" == "" && "$Symbol2" == "" ]]; then
myFloat=${myFloat:0:10}
fi
echo $myFloat
