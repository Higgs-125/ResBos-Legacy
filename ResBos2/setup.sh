#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
lsetup "cmake 3.11.0"
export LHAPDFSYS=/lustre/AtlUser/yfu/LHAPDF
export PATH=${PATH}:/lustre/AtlUser/yfu/LHAPDF/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/lustre/AtlUser/yfu/LHAPDF/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/lustre/AtlUser/yfu/pku_resbos/ScaleUncertainty/resbos2_05142019/build/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/lustre/AtlUser/yfu/pku_resbos/HOPPET/lib
export PATH=${PATH}:/lustre/AtlUser/yfu/pku_resbos/HOPPET/bin
#export PATH=${PATH}:/home/resbos/resbos2/build/bin
