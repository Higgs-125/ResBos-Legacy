#!/bin/bash
WORKDIR=$PWD
CONVDIR=/ustcfs2/yfu/pku_resbos/IYFitting/ResBos2
multicore=0
localrun=0
RunNonPertFit=0
PDFNumber=7
UseLatestPara=0

onlyCentral=0
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=165
InitialProcess=151
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

AORDER=4
BORDER=3
CORDER=2
SCALECHOICE=MT
NonPertFormat=BLNY

WHICHPDF=${MYPDF}_${SCALECHOICE}_w${AORDER}${BORDER}${CORDER}_pds
NJobs=`cat inp/q_grid_0.inp |wc -l`
NBosons=4

cd $WORKDIR
if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

for((Boson=1;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $Boson -eq 1 ]; then
WHICHBOSON=ZU
myBOSON=zu
elif [ $Boson -eq 2 ]; then
WHICHBOSON=ZD
myBOSON=zd
elif [ $Boson -eq 3 ]; then
WHICHBOSON=WPlus
myBOSON=wp
elif [ $Boson -eq 4 ]; then
WHICHBOSON=WMinus
myBOSON=wm
fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFDIR=${PDFFile}0${k}_${myBOSON}_${NonPertFormat}
NonPertDIR=${PDFFile}0${k}_NonPertFit_${NonPertFormat}
SETNUMBER=${k}
else
PDFDIR=${PDFFile}${k}_${myBOSON}_${NonPertFormat}
NonPertDIR=${PDFFile}${k}_NonPertFit_${NonPertFormat}
SETNUMBER=${k}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFDIR=${PDFFile}00_${myBOSON}_${NonPertFormat}
NonPertDIR=${PDFFile}00_NonPertFit_${NonPertFormat}
SETNUMBER=0
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleVari=908
ScaleDIR=Scale908
if [ $UseLatestPara -eq 1 -a $AORDER -eq 4 ]; then
ScaleVari=977
ScaleDIR=Scale977
fi

fi

if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR
fi
if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR
fi

echo "This Job is for $PDFDIR $ScaleDIR"

if [ $RunNonPertFit -eq 1 ]; then

#cd $WORKDIR/${WHICHPDF}/${NonPertDIR}/$ScaleDIR
cd /ustcfs2/yfu/pku_resbos/IYFitting/ResBos2/${WHICHPDF}/${NonPertDIR}/$ScaleDIR
source ~/pku_resbos/FrameWork/script/getNonPertParAndErr.sh 1

else

NonPertFormat=BLNY
G1NUMBER=0.21
G2NUMBER=0.68
G3NUMBER=-0.126
G4NUMBER=0.0
G5NUMBER=0.0
G6NUMBER=0.0

#NonPertFormat=IY6
#G1NUMBER=0.98962
#G2NUMBER=0.072164
#G3NUMBER=0.0
#G4NUMBER=-0.1208
#G5NUMBER=1.0
#G6NUMBER=3.0439

#NonPertFormat=IY6
#G1NUMBER=1.0384
#G2NUMBER=0.0579
#G3NUMBER=0.0
#G4NUMBER=-0.141
#G5NUMBER=1.0
#G6NUMBER=0.8852

if [ $UseLatestPara -eq 1 -a $AORDER -eq 4 ]; then
NonPertFormat=BLNY
G1NUMBER=0.34483
G2NUMBER=0.49333
G3NUMBER=-0.1858
fi

fi

##################################
source /home/yfu/pku_resbos/FrameWork/script/ScaleConfig.sh $ScaleVari
##################################

for((n=1;n<=$NJobs;n++)); do
cd $WORKDIR
QVALUE=`cat inp/q_grid_0.inp|head -${n}|tail -1`
echo "Q is " $QVALUE
if [ ! -d "$WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n/" ]; then
    mkdir $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
fi

cp 00resbos.config $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n

cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n

sed -i "s/MYPDF/$MYPDF/g" 00resbos.config
sed -i "s/SETNUMBER/$SETNUMBER/g" 00resbos.config
sed -i "s/AORDER/$AORDER/g" 00resbos.config
sed -i "s/BORDER/$BORDER/g" 00resbos.config
sed -i "s/CORDER/$CORDER/g" 00resbos.config
sed -i "s/C1SCALE/$C1SCALE/g" 00resbos.config
sed -i "s/C2SCALE/$C2SCALE/g" 00resbos.config
sed -i "s/C3SCALE/$C3SCALE/g" 00resbos.config
sed -i "s/MUFSCALE/$MUFSCALE/g" 00resbos.config
sed -i "s/MURSCALE/$MURSCALE/g" 00resbos.config
sed -i "s/SCALECHOICE/$SCALECHOICE/g" 00resbos.config
sed -i "s/WHICHBOSON/$WHICHBOSON/g" 00resbos.config
sed -i "s/QVALUE/$QVALUE/g" 00resbos.config
sed -i "s/G1NUMBER/$G1NUMBER/g" 00resbos.config
sed -i "s/G2NUMBER/$G2NUMBER/g" 00resbos.config
sed -i "s/G3NUMBER/$G3NUMBER/g" 00resbos.config
sed -i "s/G4NUMBER/$G4NUMBER/g" 00resbos.config
sed -i "s/G5NUMBER/$G5NUMBER/g" 00resbos.config
sed -i "s/G6NUMBER/$G6NUMBER/g" 00resbos.config
#sed -i "s/G7NUMBER/$G7NUMBER/g" 00resbos.config
sed -i "s/NonPertFormat/$NonPertFormat/g" 00resbos.config
cat 00resbos.config > resbos.config

if [ ! -d "ConvGrids/" ]; then
    mkdir ConvGrids
fi
if [ ! -d "Grids/" ]; then
    mkdir Grids
fi

cd ConvGrids
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1.out ${MYPDF}_${SETNUMBER}_Conv_C1.out
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1P1.out ${MYPDF}_${SETNUMBER}_Conv_C1P1.out
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1P1P1.out ${MYPDF}_${SETNUMBER}_Conv_C1P1P1.out
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C1P2.out ${MYPDF}_${SETNUMBER}_Conv_C1P2.out
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C2.out ${MYPDF}_${SETNUMBER}_Conv_C2.out
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_C2P1.out ${MYPDF}_${SETNUMBER}_Conv_C2P1.out
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_G1.out ${MYPDF}_${SETNUMBER}_Conv_G1.out
#ln -s ${CONVDIR}/ConvGrids/${MYPDF}_${SETNUMBER}_Conv_G1P1.out ${MYPDF}_${SETNUMBER}_Conv_G1P1.out
cd ..

if [ $localrun -eq 1 ]; then
if [ $multicore -eq 1 ]; then

totalJob=`ps -ef|grep resbos|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
while [ $totalJob -gt 90 ]; do
  sleep 5
  totalJob=`ps -ef|grep resbos|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
done

nohup ./resbos &

else
./resbos
fi
echo $PDFDIR $ScaleDIR has been generated.

else ###submit condor job

totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
while [ $totalJob -gt 1960 ] ; do
  sleep 30
  totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
done

cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
cat >>mySUB_ResBos2_${MYPDF}_${myBOSON}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = (machine != "wn101.hep.ustc.edu.cn" && machine != "wn103.hep.ustc.edu.cn" && machine != "wn104.hep.ustc.edu.cn" && machine != "wn106.hep.ustc.edu.cn" && machine != "wn107.hep.ustc.edu.cn" && machine != "wn108.hep.ustc.edu.cn" && machine != "wn109.hep.ustc.edu.cn" && machine != "wn110.hep.ustc.edu.cn" && machine != "wn111.hep.ustc.edu.cn" && machine != "wn112.hep.ustc.edu.cn" && machine != "wn113.hep.ustc.edu.cn" && machine != "wn114.hep.ustc.edu.cn" && machine != "wn116.hep.ustc.edu.cn")
executable                      = myANA.sh


transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${k}_${n}.condor.log
output                          = JOBINDEX_${k}_${n}.stdout
error                           = JOBINDEX_${k}_${n}.stderr

#notification                    = NEVER

queue
EOF
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "cmake 3.14.0"
export LHAPDFSYS=/home/yfu/LHAPDF
export PATH=/home/yfu/LHAPDF/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/LHAPDF/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/lib64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/yfu/HOPPET/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/yfu/BAT/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/HOPPET/bin:${PATH}
export PATH=/home/yfu/BAT/bin:${PATH}
export LD_LIBRARY_PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/_deps/yaml-cpp-build:${LD_LIBRARY_PATH}
export PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/bin:${PATH}
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB$n
resbos
###############################################
EOF
chmod +x myANA.sh

echo one job has been submitted
condor_submit mySUB_ResBos2_${MYPDF}_${myBOSON}_${k}_${n}

fi

done ##done job

echo $PDFDIR $ScaleDIR has been submitted

done ##done process

done ##done boson

cd $WORKDIR
