#!/bin/bash
WORKDIR=$PWD
CONVDIR=/lustre/AtlUser/yfu/pku_resbos/NewResBos2Test/run/ResBos2
multicore=0
localrun=0
PDFNumber=7

onlyCentral=1
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=165
InitialProcess=151
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

AORDER=4
BORDER=3
CORDER=2
SCALECHOICE=MT
NonPertFormat=IY6
NParameters=6

WHICHPDF=${MYPDF}_${SCALECHOICE}_w${AORDER}${BORDER}${CORDER}_pds
NJobs=`cat inp/q_grid_0.inp |wc -l`
NBosons=2

cd $WORKDIR
if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFDIR=${PDFFile}0${k}_NonPertFit_${NonPertFormat}
SETNUMBER=${k}
else
PDFDIR=${PDFFile}${k}_NonPertFit_${NonPertFormat}
SETNUMBER=${k}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFDIR=${PDFFile}00_NonPertFit_${NonPertFormat}
SETNUMBER=0
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleVari=908
ScaleDIR=Scale908
fi

if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR
fi
if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR
fi

##################################
source /home/yfu/pku_resbos/FrameWork/script/ScaleConfig.sh $ScaleVari
##################################

cd $WORKDIR

cp 00fitting.config experiments.yml parameters.yml compare.cpp figure.cpp $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
cp -r data $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR

cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR

sed -i "s/MYPDF/$MYPDF/g" 00fitting.config
sed -i "s/SETNUMBER/$SETNUMBER/g" 00fitting.config
sed -i "s/AORDER/$AORDER/g" 00fitting.config
sed -i "s/BORDER/$BORDER/g" 00fitting.config
sed -i "s/CORDER/$CORDER/g" 00fitting.config
sed -i "s/C1SCALE/$C1SCALE/g" 00fitting.config
sed -i "s/C2SCALE/$C2SCALE/g" 00fitting.config
sed -i "s/C3SCALE/$C3SCALE/g" 00fitting.config
sed -i "s/MUFSCALE/$MUFSCALE/g" 00fitting.config
sed -i "s/MURSCALE/$MURSCALE/g" 00fitting.config
sed -i "s/SCALECHOICE/$SCALECHOICE/g" 00fitting.config
sed -i "s/NonPertFormat/$NonPertFormat/g" 00fitting.config
sed -i "s/NParameters/$NParameters/g" 00fitting.config
cat 00fitting.config > fitting.config

if [ ! -d "ConvGrids/" ]; then
    mkdir ConvGrids
fi
if [ ! -d "Grids/" ]; then
    mkdir Grids
fi

cd ConvGrids
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_C1.out ${MYPDF}_${SETNUMBER}_Conv_C1.out
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_C1P1.out ${MYPDF}_${SETNUMBER}_Conv_C1P1.out
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_C1P1P1.out ${MYPDF}_${SETNUMBER}_Conv_C1P1P1.out
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_C1P2.out ${MYPDF}_${SETNUMBER}_Conv_C1P2.out
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_C2.out ${MYPDF}_${SETNUMBER}_Conv_C2.out
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_C2P1.out ${MYPDF}_${SETNUMBER}_Conv_C2P1.out
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_G1.out ${MYPDF}_${SETNUMBER}_Conv_G1.out
#ln -s ${CONVDIR}/ConvGridsNonPert/${MYPDF}_${SETNUMBER}_Conv_G1P1.out ${MYPDF}_${SETNUMBER}_Conv_G1P1.out
cd ..

if [ $localrun -eq 1 ]; then
if [ $multicore -eq 1 ]; then

totalJob=`ps -ef|grep resbos|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#while [ $totalJob -gt 15 ]; do
#  sleep 5
#  totalJob=`ps -ef|grep resbos|grep -v grep|awk '{print $1}'|grep ${USER}|wc -l`
#done

nohup ./NonPertFit &

else
./NonPertFit
fi
echo $PDFDIR $ScaleDIR has been generated.

else ###submit condor job

totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
while [ $totalJob -gt 1960 ] ; do
  sleep 30
  totalJob=`condor_q ${USER}|grep ${USER}|wc -l`
done

cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
cat >>mySUB_NonPertFit_${MYPDF}_${myBOSON}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = (machine != "wn101.hep.ustc.edu.cn" && machine != "wn103.hep.ustc.edu.cn" && machine != "wn106.hep.ustc.edu.cn" && machine != "wn107.hep.ustc.edu.cn" && machine != "wn108.hep.ustc.edu.cn" && machine != "wn109.hep.ustc.edu.cn" && machine != "wn110.hep.ustc.edu.cn" && machine != "wn111.hep.ustc.edu.cn" && machine != "wn112.hep.ustc.edu.cn" && machine != "wn113.hep.ustc.edu.cn" && machine != "wn114.hep.ustc.edu.cn" && machine != "wn116.hep.ustc.edu.cn")
executable                      = myANA.sh

transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${k}_${n}.condor.log
output                          = JOBINDEX_${k}_${n}.stdout
error                           = JOBINDEX_${k}_${n}.stderr

#notification                    = NEVER

accounting_group                = long

queue
EOF
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "cmake 3.14.0"
export LHAPDFSYS=/home/yfu/LHAPDF
export PATH=/home/yfu/LHAPDF/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/LHAPDF/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/lib64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/yfu/HOPPET/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/yfu/BAT/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/HOPPET/bin:${PATH}
export PATH=/home/yfu/BAT/bin:${PATH}
export LD_LIBRARY_PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/_deps/yaml-cpp-build:${LD_LIBRARY_PATH}
export PATH=/ustcfs2/yfu/pku_resbos/SourceCode/resbos2/build/bin:${PATH}
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR
NonPertFit
###############################################
EOF
chmod +x myANA.sh

echo one job has been submitted
condor_submit mySUB_NonPertFit_${MYPDF}_${myBOSON}_${k}_${n}

fi

echo $PDFDIR $ScaleDIR has been submitted

done ##done process

cd $WORKDIR
