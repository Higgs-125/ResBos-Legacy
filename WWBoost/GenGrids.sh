#!/bin/bash
WORKDIR=$PWD
onlyCentral=0
RunNonPertFit=0
isKinCorr=1
KinCorr=1
UseLatestPara=0

PDFNumber=1
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
############################################################################
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=15
InitialProcess=1
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

ScaleChoice1=1

if [ $ScaleChoice1 -eq 1 ]; then
SCALECHOICE=MT
elif [ $ScaleChoice1 -eq 0 ]; then
SCALECHOICE=Q
fi

WHICHPDF=${MYPDF}_${SCALECHOICE}_pds

NJobs=1

cd $WORKDIR

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFNAME=${PDFFile}0${k}.pds
PDFDIR=${PDFFile}0${k}
SETNUMBER=$k
else
PDFNAME=${PDFFile}${k}.pds
PDFDIR=${PDFFile}${k}
SETNUMBER=$k
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
PDFNAME=${PDFFile}00.pds
PDFDIR=${PDFFile}00
SETNUMBER=0
elif [ $Process -eq 2 ]; then
cd $WORKDIR
ScaleVari=908
ScaleDIR=Scale908
if [ $UseLatestPara -eq 1 ]; then
ScaleVari=977
ScaleDIR=Scale977
fi
fi

##################################
source /home/yfu/pku_resbos/FrameWork/script/ScaleConfig.sh $ScaleVari
##################################

if [ ! -d "$WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/Grids" ]; then
    mkdir $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/Grids
fi

#if [ ! -d "$WORKDIR/${WHICHPDF}/NLO/$PDFDIR/$ScaleDIR/Grids" ]; then
#    mkdir $WORKDIR/${WHICHPDF}/NLO/$PDFDIR/$ScaleDIR/Grids
#fi

cp convert.py $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/Grids
#cp convert.py generate.py $WORKDIR/${WHICHPDF}/NLO/$PDFDIR/$ScaleDIR/Grids

for((n=1;n<=$NJobs;n++)); do

cd $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/Grids
python3 convert.py $WORKDIR/${WHICHPDF}/LO/$PDFDIR/$ScaleDIR/JOB$n/WW_jet_lo_CT14HER_1____1____pt34cut13TeV.top WW_lo_13Tev_binned_${n}.dat

done

echo $PDFDIR $ScaleDIR has finished

done

cd $WORKDIR

