import argparse


def read_plot(fileobj, outobj):
    for _ in range(4):
        fileobj.readline()
    title = fileobj.readline().split('"')[-2]
    outobj.write("  {}\n".format(title))
    for _ in range(6):
        fileobj.readline()
    line = fileobj.readline()
    while 'PLOT' not in line:
        if 'NaN' not in line:
            outobj.write(line)
        line = fileobj.readline()
    while 'NEW PLOT' not in fileobj.readline():
        continue

    return fileobj.readline()


def main():
    parser = argparse.ArgumentParser(description='Convert filetype from MCFM to angular k-factor')
    parser.add_argument('input', help='Input file name')
    parser.add_argument('out', help='Output file name')
    args = parser.parse_args()

    infile = args.input
    outfile = args.out

    with open(infile) as fileobj:
        while 'SET WINDOW' not in fileobj.readline():
            pos = fileobj.tell()
        fileobj.seek(pos)
        
        with open(outfile, 'w') as outobj:
            while True:
                line = read_plot(fileobj, outobj)
                if line == '':
                    break
                outobj.write('\n')


if __name__ == '__main__':
    main()
