#!/bin/bash
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "cmake 3.14.0"
lsetup "python 3.7.4-x86_64-centos7"
unset PYTHONPATH
unset PYTHONHOME
#python3 -m pip install --upgrade pip
#python3 -m pip install --user numpy
#python3 -m pip install --user matplotlib
#python3 -m pip install --user pandas
#python3 -m pip install --user pyyaml
#python3 -m pip install --user scipy
export LHAPDFSYS=/home/yfu/LHAPDF
export PATH=/home/yfu/LHAPDF/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/LHAPDF/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/home/yfu/public/pku_resbos/SourceCode/MCFM_Josh/qcdloop-2.0.2/local/lib:${LD_LIBRARY_PATH}
export PATH=/home/yfu/MPI/bin:${PATH}
export LD_LIBRARY_PATH=/home/yfu/MPI/lib:${LD_LIBRARY_PATH}
export MANPATH=/home/yfu/MPI/share/man:${MANPATH}
