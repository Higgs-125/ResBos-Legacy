#!/bin/bash
WORKDIR=$PWD
GridDIR=/lustre/AtlUser/yfu/pku_resbos/gridfile
multicore=0
localrun=0
WConfig=2   ###1: w321  2: w432  0: delsig  -1: asy
YConfig=2   ###1: y1    2: y2    0: onlyW   -1: asy  ##For NLO calculation, please usage WConfig=0 YConfig=0, and then WConfig=-1 YConfig=-1, and hadd the result.
onlyCentral=1

PDFNumber=7
ECM=13TeV
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 1
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi
############################################################################
NJobs=10

Process=$1
if [ $Process -eq 1 ]; then
    echo Running scale variation.
    NProcess=15
    InitialProcess=1
elif [ $Process -eq 2 ]; then
    echo Running PDF uncertainty.
    NProcess=${NumberOfPDFSet}
    InitialProcess=0
fi

NBosons=2

IsReweight=$2
if [ $IsReweight -eq 1 ]; then
    IWGT=-1
    if [ $Process -eq 1 ]; then
        NProcess=1
        InitialProcess=1
    elif [ $Process -eq 2 ]; then
        NProcess=0
        InitialProcess=0
    fi
elif [ $IsReweight -eq 2 ]; then
    IWGT=-2
    if [ $Process -eq 1 ]; then
        InitialProcess=2
    elif [ $Process -eq 2 ]; then
        InitialProcess=1
    fi
elif [ $IsReweight -eq 0 ]; then
    IWGT=0
fi

cd $WORKDIR
if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

for((Boson=1;Boson<=$NBosons;Boson++)); do
########Set Boson Type############
if [ $Boson -eq 1 ]; then
    myBOSON=zu
elif [ $Boson -eq 2 ]; then
    myBOSON=zd
elif [ $Boson -eq 3 ]; then
    myBOSON=wp
elif [ $Boson -eq 4 ]; then
    myBOSON=wm
fi

echo " "
echo ${myBOSON} production:
echo " "

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=${NProcess};k++)); do
if [ $k -lt 10 ]; then
    PDFDIR=${PDFFile}0${k}_${myBOSON}
else
    PDFDIR=${PDFFile}${k}_${myBOSON}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
    cd $WORKDIR
    PDFDIR=${PDFFile}00_${myBOSON}

elif [ $Process -eq 2 ]; then
    cd $WORKDIR
    ScaleDIR=Scale908
fi

if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR
fi
if [ ! -d "$WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR/" ]; then
    mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/$ScaleDIR
fi

echo " "
echo $PDFDIR $ScaleDIR
echo " "

for((n=1;n<=$NJobs;n++)); do
STWINPUT=2315
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR

if [ ! -d "$WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}/" ]; then
    mkdir $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
fi

cd $WORKDIR
cp resbos_root resbos.in $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
if [ $IsReweight -eq 0 ]; then
    RANDOMSEED=$[${RANDOM}+1*${k}+10*${n}]
else
    #RANDOMSEED=14640
    RANDOMSEED=$[10000+10*${n}]
fi
sed -i "s/RANDOMSEED/$RANDOMSEED/g" resbos.in
sed -i "s/IWGT/$IWGT/g" resbos.in
sed -i "s/ISYGRID/$ISYGRID/g" resbos.in

if [ $IsReweight -eq 2 ]; then
    if [ $Process -eq 1 ]; then
        ln -s ../../../${PDFFile}00_${myBOSON}/Scale901/JOB${n}/weights.dat weights.dat
        cp ../../../${PDFFile}00_${myBOSON}/Scale901/JOB${n}/resbos.root resbos.root
    elif [ $Process -eq 2 ]; then
        ln -s ../../../${PDFFile}00_${myBOSON}/Scale908/JOB${n}/weights.dat weights.dat
        cp ../../../${PDFFile}00_${myBOSON}/Scale908/JOB${n}/resbos.root resbos.root
    fi
fi

#sed -i "s/STWINPUT/$STWINPUT/g" resbosm.f

#############refresh the W-piece grid path and Y-piece grid path##############
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 0
##############################################################################

if [ $PDFNumber -eq 1 ]; then
    if [ $WConfig -eq 2 ]; then
        echo WARNING!! There is no w432 gridfile in old path structure.
    fi

    ln -s ${GridDIR}/${MYPDF}/${myBOSON}/w321/${PDFDIR}_w321.out legacy_w321.out

    if [ $YConfig -eq 1 ]; then
        ln -s ${GridDIR}/${MYPDF}/${myBOSON}/y/${PDFDIR}_y.out legacy_yk.out
    elif [ $YConfig -eq 2 ]; then
        ln -s ${GridDIR}/${MYPDF}/${myBOSON}/yk/${PDFDIR}_yk.out legacy_yk.out
    fi
else
    ln -s $WGridPath legacy_w321.out
    ln -s $YGridPath legacy_yk.out
fi

#ln -s $WORKDIR/../ResBos2/${MYPDF}_pds/${PDFDIR}/$ScaleDIR/y_1/JOB1/Grids/ResBos_GridFile_0.out legacy_w321.out
#ln -s $WORKDIR/../../old_legacy/${MYPDF}_pds/${PDFDIR}/${ScaleDIR}/JOB1/legacy.out legacy_w321.out
#ln -s $WORKDIR/../../get_yk/${PDFDIR}_${ScaleDIR}_yk.out legacy_yk.out

if [ $localrun -eq 1 ]; then

if [ $multicore -eq 1 ]; then
    nohup ./resbos_root &
else
    ./resbos_root
fi

else ##submit condor job

cat >>mySUB_ResBos_${WConfig}_${YConfig}_${myBOSON}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = (machine == "bl-3-01.hep.ustc.edu.cn" || machine == "bl-3-02.hep.ustc.edu.cn" || machine == "bl-3-03.hep.ustc.edu.cn" || machine == "bl-3-04.hep.ustc.edu.cn" || machine == "bl-3-06.hep.ustc.edu.cn" || machine == "bl-4-01.hep.ustc.edu.cn" || machine == "bl-4-02.hep.ustc.edu.cn" || machine == "bl-4-05.hep.ustc.edu.cn" || machine == "bl-4-07.hep.ustc.edu.cn" || machine == "bl-4-09.hep.ustc.edu.cn" || machine == "bl-4-10.hep.ustc.edu.cn" || machine == "bl-5-01.hep.ustc.edu.cn" || machine == "bl-5-02.hep.ustc.edu.cn" || machine == "bl-5-03.hep.ustc.edu.cn" || machine == "bl-5-05.hep.ustc.edu.cn" || machine == "bl-5-06.hep.ustc.edu.cn" || machine == "bl-5-07.hep.ustc.edu.cn" || machine == "bl-5-08.hep.ustc.edu.cn" || machine == "bl-5-10.hep.ustc.edu.cn" || machine == "bl-5-11.hep.ustc.edu.cn" || machine == "bl-5-13.hep.ustc.edu.cn" || machine == "bl-5-14.hep.ustc.edu.cn")
executable                      = myANA.sh


transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${k}_${n}.condor.log
output                          = JOBINDEX_${k}_${n}.stdout
error                           = JOBINDEX_${k}_${n}.stderr

#notification                    = NEVER

queue
EOF
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupROOT 6.12.06-x86_64-slc6-gcc62-opt
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/lustre/AtlUser/yfu/LHAPDF/lib
cd $WORKDIR/$WHICHPDF/$PDFDIR/$ScaleDIR/JOB${n}
#make
./resbos_root
###############################################
EOF
chmod +x myANA.sh

condor_submit mySUB_ResBos_${WConfig}_${YConfig}_${myBOSON}_${k}_${n}

fi

cd $WORKDIR/

done

done ##done process

done ##done boson

cd $WORKDIR/
