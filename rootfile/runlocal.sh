#!/bin/bash
WORKDIR=$PWD
multicore=0
localrun=0
IsReweight=1
WConfig=0   ###1: w321  2: w432  0: FixedOrder
YConfig=0   ###1: y1    2: y2    0: onlyW
onlyCentral=1

PDFNumber=2
ECM=8TeV
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 1
############################################################################
if [ $onlyCentral -eq 1 ]; then
    NumberOfPDFSet=0
fi

Process=$1
if [ $Process -eq 1 ]; then
echo Running scale variation.
NProcess=15
InitialProcess=1
elif [ $Process -eq 2 ]; then
echo Running PDF uncertainty.
NProcess=${NumberOfPDFSet}
InitialProcess=0
fi

NJobs=10

if [ $WConfig -eq 0 ]; then
    WHICHPDF=${MYPDF}_${ECM}_FixedOrder
fi

if [ ! -d "$WHICHPDF/" ]; then
    mkdir $WHICHPDF
fi

##########Define Scale and PDF name####################
for((k=${InitialProcess};k<=$NProcess;k++)); do
if [ $k -lt 10 ]; then
PDFDIR=${PDFFile}0${k}
else
PDFDIR=${PDFFile}${k}
fi
ScaleVari=$[900+${k}]
ScaleDIR=Scale${ScaleVari}

##########build directory##############################
if [ $Process -eq 1 ]; then
cd $WORKDIR
mkdir $WORKDIR/${WHICHPDF}/$ScaleDIR
PDFDIR=${PDFFile}00
elif [ $Process -eq 2 ]; then
cd $WORKDIR
mkdir $WORKDIR/${WHICHPDF}/$PDFDIR
ScaleDIR=Scale908
fi

#########Running#######################################
for((n=1;n<=$NJobs;n++)); do
cd $WORKDIR
###############Run Scale####################
if [ $Process -eq 1 ]; then

mkdir $WORKDIR/${WHICHPDF}/$ScaleDIR/JOB$n
cp loopMC.exe $WORKDIR/${WHICHPDF}/$ScaleDIR/JOB$n
cd $WORKDIR/${WHICHPDF}/$ScaleDIR/JOB$n
echo $WORKDIR/../samplefile/${WHICHPDF}/${PDFDIR}_zu/${ScaleDIR}/JOB${n}/resbos.root >> data.list
echo $WORKDIR/../samplefile/${WHICHPDF}/${PDFDIR}_zd/${ScaleDIR}/JOB${n}/resbos.root >> data.list

if [ $WConfig -eq 0 ]; then
    rm data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_delsig/${PDFDIR}_zu/${ScaleDIR}/JOB${n}/resbos.root >> data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_delsig/${PDFDIR}_zd/${ScaleDIR}/JOB${n}/resbos.root >> data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_asy/${PDFDIR}_zu/${ScaleDIR}/JOB${n}/resbos.root >> data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_asy/${PDFDIR}_zd/${ScaleDIR}/JOB${n}/resbos.root >> data.list
fi

if [ $IsReweight -eq 1 ]; then

if [ $k -lt 2 ]; then
echo 1 > Weight.dat
else
echo 2 > Weight.dat
fi

else

echo 1 > Weight.dat

fi

cd $WORKDIR/${WHICHPDF}/$ScaleDIR/JOB$n

##############Run PDF#######################
elif [ $Process -eq 2 ]; then

mkdir $WORKDIR/${WHICHPDF}/$PDFDIR/JOB$n
cp loopMC.exe $WORKDIR/${WHICHPDF}/$PDFDIR/JOB$n
cd $WORKDIR/${WHICHPDF}/$PDFDIR/JOB$n

echo $WORKDIR/../samplefile/${WHICHPDF}/${PDFDIR}_zu/${ScaleDIR}/JOB${n}/resbos.root >> data.list
echo $WORKDIR/../samplefile/${WHICHPDF}/${PDFDIR}_zd/${ScaleDIR}/JOB${n}/resbos.root >> data.list

if [ $WConfig -eq 0 ]; then
    rm data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_delsig/${PDFDIR}_zu/${ScaleDIR}/JOB${n}/resbos.root >> data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_delsig/${PDFDIR}_zd/${ScaleDIR}/JOB${n}/resbos.root >> data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_asy/${PDFDIR}_zu/${ScaleDIR}/JOB${n}/resbos.root >> data.list
    echo $WORKDIR/../samplefile/${MYPDF}_${ECM}_asy/${PDFDIR}_zd/${ScaleDIR}/JOB${n}/resbos.root >> data.list
fi

if [ $IsReweight -eq 1 ]; then

if [ $k -lt 1 ]; then
echo 1 > Weight.dat
else
echo 2 > Weight.dat
fi

else

echo 1 > Weight.dat

fi

cd $WORKDIR/${WHICHPDF}/$PDFDIR/JOB$n

fi

if [ $localrun -eq 1 ]; then

if [ $multicore -eq 1 ]; then
nohup ./loopMC.exe data.list MC &
else
./loopMC.exe data.list MC
echo $PDFDIR $ScaleDIR has finished
fi

else

cat >>mySUB_Analysis_${WConfig}_${YConfig}_${k}_${n}  <<EOF
universe                        = vanilla
requirements                    = (machine == "bl-3-01.hep.ustc.edu.cn" || machine == "bl-3-02.hep.ustc.edu.cn" || machine == "bl-3-03.hep.ustc.edu.cn" || machine == "bl-3-04.hep.ustc.edu.cn" || machine == "bl-3-06.hep.ustc.edu.cn" || machine == "bl-4-01.hep.ustc.edu.cn" || machine == "bl-4-02.hep.ustc.edu.cn" || machine == "bl-4-05.hep.ustc.edu.cn" || machine == "bl-4-07.hep.ustc.edu.cn" || machine == "bl-4-09.hep.ustc.edu.cn" || machine == "bl-4-10.hep.ustc.edu.cn" || machine == "bl-5-01.hep.ustc.edu.cn" || machine == "bl-5-02.hep.ustc.edu.cn" || machine == "bl-5-03.hep.ustc.edu.cn" || machine == "bl-5-05.hep.ustc.edu.cn" || machine == "bl-5-06.hep.ustc.edu.cn" || machine == "bl-5-07.hep.ustc.edu.cn" || machine == "bl-5-08.hep.ustc.edu.cn" || machine == "bl-5-10.hep.ustc.edu.cn" || machine == "bl-5-11.hep.ustc.edu.cn" || machine == "bl-5-13.hep.ustc.edu.cn" || machine == "bl-5-14.hep.ustc.edu.cn")
executable                      = myANA.sh


transfer_output                 = true
transfer_error                  = true
transfer_executable             = true

should_transfer_files           = IF_NEEDED

#run v15 on SL5 nodes
#+SL_START                       = 5

when_to_transfer_output         = ON_EXIT

log                             = JOBINDEX_${n}.condor.log
output                          = JOBINDEX_${n}.stdout
error                           = JOBINDEX_${n}.stderr

#notification                    = NEVER

queue
EOF
cat >>myANA.sh  <<EOF
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupROOT 6.04.16-x86_64-slc6-gcc49-opt 
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/lustre/AtlUser/yfu/LHAPDF/lib
Process=${Process}
if [ $Process -eq 1 ]; then
cd $WORKDIR/${WHICHPDF}/$ScaleDIR/JOB$n
elif [ $Process -eq 2 ]; then
cd $WORKDIR/${WHICHPDF}/$PDFDIR/JOB$n
fi
./loopMC.exe data.list MC
###############################################
EOF
chmod +x myANA.sh

condor_submit mySUB_Analysis_${WConfig}_${YConfig}_${k}_${n}

fi

done
done
cd $WORKDIR
