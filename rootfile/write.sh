#!/bin/bash
WORKDIR=$PWD
ListName=theory_CT14nnlo_w0.list
WConfig=0   ###1: w321  2: w432  0: FixedOrder
YConfig=0   ###1: y1    2: y2    0: onlyW

PDFNumber=2
ECM=8TeV
# PDFNumber = 1: CT14HEAR2NNLO
# PDFNumber = 2: CT14nnlo
# PDFNumber = 3: MMHT2014nnlo68cl
######################Initialize the PDF Name###############################
source /home/yfu/pku_resbos/FrameWork/script/PDFInformation.sh $PDFNumber
source /home/yfu/pku_resbos/FrameWork/script/ResBosConfig.sh $WConfig $YConfig 1
############################################################################
if [ -f "$ListName" ]; then
    rm $ListName
fi

NJobs=2

if [ $WConfig -eq 0 ]; then
    WHICHPDF=${MYPDF}_${ECM}_FixedOrder
fi
Statistic=$[50*${NJobs}]
Statistic=${Statistic}M

EtaRegion=CF

for((k=0;k<=${NumberOfPDFSet};k++)); do
if [ $k -lt 10 ]; then
PDFDIR=${PDFFile}0${k}
else
PDFDIR=${PDFFile}${k}
fi

echo $WORKDIR/$WHICHPDF/$PDFDIR/$Statistic/MC_results.root >> ${ListName}
done
