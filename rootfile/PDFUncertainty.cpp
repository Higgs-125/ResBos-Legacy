void PDFUncertainty()
{
 int NPDFs = 56;
 int iflag = 1;
 TString histName = "AFB";
 TString FName = "FZmass_CF";
 TString BName = "BZmass_CF";
 TString saveName = histName + "_PDFError.root";

 ifstream infile;
  infile.open("theory.list", ios::in);

 TString rootName;

 TFile* file[NPDFs + 1];
 TH1D *h1[NPDFs + 1];
 TH2D *h2[NPDFs + 1];
 TH1D *PDFPlot1D[NPDFs + 1];
 TH2D *PDFPlot2D[NPDFs + 1];

 TH1D *FZmass[NPDFs + 1];
 TH1D *BZmass[NPDFs + 1];
 double xbins[2] = {80, 100};
 void AFBFunction(TH1D* FZmass, TH1D* BZmass, TH1D* &Draw_AFB, int i);

 double Unc2 = 0;
 double Unc = 0;

 TFile* writefile = new TFile(saveName, "RECREATE");

 for(int i = 0; i < NPDFs + 1; i++){
  infile>>rootName;
  file[i] = new TFile(rootName);

//  if(iflag == 1) h1[i] = (TH1D *)file[i]->Get(histName);
  if(iflag == 2) h2[i] = (TH2D *)file[i]->Get(histName);

  FZmass[i] = (TH1D *)file[i]->Get(FName);
  BZmass[i] = (TH1D *)file[i]->Get(BName);

  FZmass[i] = (TH1D *)FZmass[i]->Rebin(1, "FZmass", xbins);
  BZmass[i] = (TH1D *)BZmass[i]->Rebin(1, "BZmass", xbins);
//cout<<"1"<<endl;
  AFBFunction(FZmass[i], BZmass[i], h1[i], i);
 }

 TH1D *PDFCentral1D;
 TH2D *PDFCentral2D;

 TH1D *PDFError1D;
 TH2D *PDFError2D;

 TH1D *PDFStatError1D;
 TH2D *PDFStatError2D;

 writefile->cd();
 if(iflag == 1){
    for(int i = 1; i < NPDFs + 1;i++){
        PDFPlot1D[i] = (TH1D *)h1[i]->Clone(histName + "_PDF" + (long)(0 + i));
    }
 }
 if(iflag == 2){
    for(int i = 1; i < NPDFs + 1;i++){
        PDFPlot2D[i] = (TH2D *)h2[i]->Clone(histName + "_PDF" + (long)(0 + i));
    }
 }


 if(iflag == 1) PDFCentral1D = (TH1D *)h1[0]->Clone(histName + "_PDFCentral");
 if(iflag == 2) PDFCentral2D = (TH2D *)h2[0]->Clone(histName + "_PDFCentral");

 if(iflag == 1) PDFError1D = (TH1D *)h1[1]->Clone(histName + "_PDFError");
 if(iflag == 2) PDFError2D = (TH2D *)h2[1]->Clone(histName + "_PDFError");

 if(iflag == 1) PDFStatError1D = (TH1D *)h1[0]->Clone(histName + "_PDFStatError");
 if(iflag == 2) PDFStatError2D = (TH2D *)h2[0]->Clone(histName + "_PDFStatError");

 if(iflag == 1) PDFStatError1D->Reset();
 if(iflag == 2) PDFStatError2D->Reset();

 if(iflag == 1){
  for(int ibin = 1; ibin <= h1[0]->GetNbinsX(); ibin++){
   PDFStatError1D->SetBinContent(ibin, PDFCentral1D->GetBinError(ibin));
  }
 }

 if(iflag == 2){
  for(int ibinx = 1; ibinx <= h2[0]->GetNbinsX(); ibinx++){
   for(int ibiny = 1; ibiny <= h2[0]->GetNbinsY(); ibiny++){
   PDFStatError2D->SetBinContent(ibinx, ibiny, PDFCentral2D->GetBinError(ibinx, ibiny));
   }
  }
 }

 if(iflag == 1){
/*  for(int ibin = 1; ibin <= h1[1]->GetNbinsX(); ibin++){
   Unc2 = 0;
   Unc = 0;
   for(int i = 1; i < NPDFs / 2 + 1; i++){
    Unc2 += (h1[2 * i]->GetBinContent(ibin) - h1[2 * i - 1]->GetBinContent(ibin)) * (h1[2 * i]->GetBinContent(ibin) - h1[2 * i - 1]->GetBinContent(ibin)) / 4;
    cout<<i<<"  "<<(h1[2 * i]->GetBinContent(ibin) - h1[2 * i - 1]->GetBinContent(ibin))<<endl;
   }
   Unc = sqrt(Unc2);
   PDFError1D->SetBinContent(ibin, Unc);
  }
*/

  for(int ibin = 1; ibin <= h1[1]->GetNbinsX(); ibin++){
    Unc2 = 0;
    Unc = 0;
    for(int i = 1; i <= NPDFs; i++){
      Unc2 += ((h1[i]->GetBinContent(ibin) - PDFCentral1D->GetBinContent(ibin)) * (h1[i]->GetBinContent(ibin) - PDFCentral1D->GetBinContent(ibin))) / 2;
      cout<<i<<"  "<<(h1[i]->GetBinContent(ibin) - PDFCentral1D->GetBinContent(ibin))<<endl;
    }
    Unc = sqrt(Unc2);
    PDFError1D->SetBinContent(ibin, Unc);
  }

 }

 if(iflag == 2){
  for(int ibinx = 1; ibinx <= h2[1]->GetNbinsX(); ibinx++){
   for(int ibiny = 1; ibiny <= h2[1]->GetNbinsY(); ibiny++){
    Unc2 = 0;
    Unc = 0;
    for(int i = 1; i < NPDFs / 2 + 1; i++){
     Unc2 += (h2[2 * i]->GetBinContent(ibinx, ibiny) - h2[2 * i - 1]->GetBinContent(ibinx, ibiny)) * (h2[2 * i]->GetBinContent(ibinx, ibiny) - h2[2 * i - 1]->GetBinContent(ibinx, ibiny)) / 4;
    }
    Unc = sqrt(Unc2);
    PDFError2D->SetBinContent(ibinx, ibiny, Unc);
   }
  }
 }  

 writefile->Write();
 writefile->Close();
}

void AFBFunction(TH1D* FZmass, TH1D* BZmass, TH1D* &Draw_AFB, int i)
{
 double Nf, Nb, AFB, NfError, NbError, AFBError;

 Draw_AFB = (TH1D *)FZmass->Clone("AFB_" + (long)(i));
 Draw_AFB->Reset();

 for (int ibin = 0; ibin < FZmass->GetNbinsX(); ibin++)
 {
  Nf = FZmass->GetBinContent(ibin + 1);
  Nb = BZmass->GetBinContent(ibin + 1);
  AFB = (Nf - Nb) / (Nf + Nb);
  
  NfError = FZmass->GetBinError(ibin + 1);
  NbError = BZmass->GetBinError(ibin + 1);
  AFBError = 2 * sqrt((Nf * NbError) * (Nf * NbError) + (Nb * NfError) * (Nb * NfError)) / ((Nf + Nb) * (Nf + Nb));
  
  Draw_AFB->SetBinContent(ibin + 1, AFB);
  Draw_AFB->SetBinError(ibin + 1, AFBError);
 }
}

